/**
 * Created by james on 8/18/15.
 */
var app = angular.module('store',[]);

app.controller('StoreController',function(){

    this.products = gems;


});

app.controller('PanelController',function(){

    this.tab = 1;

    this.selectTab =  function(x){
        this.tab = x;
    }

    this.isSelected = function(x){
        return this.tab === x;
    }

});

var gems = [
    {name:'Decahedron',
        price:2.95,
        description:'Will mesmerize you with its beauty',
        canPurchase:false,
        soldOut:true
    },
    {name:'Pentagonal Gem',
        price:5.95,
        description:'Most absolutely delightful gem even',
        canPurchase:false,
        soldOut:true
    },
    {name:'Decahedron Black Edition',
        price:2.95,
        description:'Dark beauty of the eye lashes',
        canPurchase:false,
        soldOut:false
    }
];


app.controller('menuCtrl',function($scope){
    $scope.communities = usercommunities;
});

var usercommunities = [
    {
        "id": "2",
        "useraccountid": "4",
        "communitytypeid": "2",
        "communityname": "Jamo's Private Place on the Web",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "0",
        "parentcommunity": null,
        "disabled": "N",
        "created_at": "2015-08-13 12:04:42",
        "updated_at": "2015-08-13 12:04:42",
        "deleted_at": null,
        "tree": [
            {
                "id": "14",
                "useraccountid": "4",
                "communitytypeid": "2",
                "communityname": "Real Child of Jamo's Private Place on The Web",
                "communitylogo": null,
                "communitycoverphoto": null,
                "communitylevel": "0",
                "parentcommunity": "2",
                "disabled": "N",
                "created_at": "2015-08-17 16:06:35",
                "updated_at": "2015-08-17 16:06:35",
                "deleted_at": null,
                "tree": []
            },
            {
                "id": "15",
                "useraccountid": "4",
                "communitytypeid": "1",
                "communityname": "Another child of Jamo's space on the web",
                "communitylogo": null,
                "communitycoverphoto": null,
                "communitylevel": "0",
                "parentcommunity": "2",
                "disabled": "N",
                "created_at": "2015-08-17 16:32:44",
                "updated_at": "2015-08-17 16:32:44",
                "deleted_at": null,
                "tree": []
            },
            {
                "id": "17",
                "useraccountid": "4",
                "communitytypeid": "1",
                "communityname": "A legitimate child of Jamo's space on the web",
                "communitylogo": null,
                "communitycoverphoto": null,
                "communitylevel": "0",
                "parentcommunity": "2",
                "disabled": "N",
                "created_at": "2015-08-17 16:33:53",
                "updated_at": "2015-08-17 16:33:53",
                "deleted_at": null,
                "tree": []
            },
            {
                "id": "18",
                "useraccountid": "4",
                "communitytypeid": "1",
                "communityname": "An illegitimate child of Jamo's space on the web",
                "communitylogo": null,
                "communitycoverphoto": null,
                "communitylevel": "1",
                "parentcommunity": "2",
                "disabled": "N",
                "created_at": "2015-08-17 16:35:37",
                "updated_at": "2015-08-17 16:35:37",
                "deleted_at": null,
                "tree": []
            },
            {
                "id": "19",
                "useraccountid": "4",
                "communitytypeid": "1",
                "communityname": "Yet another illegitimate child of Jamo's space on the web",
                "communitylogo": null,
                "communitycoverphoto": null,
                "communitylevel": "1",
                "parentcommunity": "2",
                "disabled": "N",
                "created_at": "2015-08-17 16:40:49",
                "updated_at": "2015-08-17 16:40:49",
                "deleted_at": null,
                "tree": [
                    {
                        "id": "20",
                        "useraccountid": "4",
                        "communitytypeid": "1",
                        "communityname": "First third gen child under Jamo's line",
                        "communitylogo": null,
                        "communitycoverphoto": null,
                        "communitylevel": "2",
                        "parentcommunity": "19",
                        "disabled": "N",
                        "created_at": "2015-08-17 16:41:31",
                        "updated_at": "2015-08-17 16:41:31",
                        "deleted_at": null,
                        "tree": []
                    }
                ]
            }
        ]
    },
    {
        "id": "3",
        "useraccountid": "4",
        "communitytypeid": "2",
        "communityname": "The Zulahoo Community",
        "communitylogo": "/communityfiles/eccbc87e4b5ce2fe28308fd9f2a7baf3/profile/zulahoo-logo-small.png",
        "communitycoverphoto": "/communityfiles/eccbc87e4b5ce2fe28308fd9f2a7baf3/profile/under-construction.gif",
        "communitylevel": "0",
        "parentcommunity": null,
        "disabled": "N",
        "created_at": "2015-08-14 14:38:18",
        "updated_at": "2015-08-17 16:30:39",
        "deleted_at": null,
        "tree": []
    },
    {
        "id": "8",
        "useraccountid": "4",
        "communitytypeid": "2",
        "communityname": "Lazarus Rising",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "0",
        "parentcommunity": null,
        "disabled": "N",
        "created_at": "2015-08-17 11:09:59",
        "updated_at": "2015-08-17 11:09:59",
        "deleted_at": null,
        "tree": []
    },
    {
        "id": "9",
        "useraccountid": "4",
        "communitytypeid": "2",
        "communityname": "Lazarus Rising 2",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "0",
        "parentcommunity": null,
        "disabled": "N",
        "created_at": "2015-08-17 11:10:48",
        "updated_at": "2015-08-17 11:10:48",
        "deleted_at": null,
        "tree": []
    },
    {
        "id": "10",
        "useraccountid": "4",
        "communitytypeid": "2",
        "communityname": "Lord of the Things",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "0",
        "parentcommunity": null,
        "disabled": "N",
        "created_at": "2015-08-17 11:11:02",
        "updated_at": "2015-08-17 11:11:02",
        "deleted_at": null,
        "tree": []
    },
    {
        "id": "11",
        "useraccountid": "4",
        "communitytypeid": "4",
        "communityname": "Irving Bikers Association",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "0",
        "parentcommunity": null,
        "disabled": "N",
        "created_at": "2015-08-17 11:11:15",
        "updated_at": "2015-08-18 08:52:21",
        "deleted_at": null,
        "tree": []
    },
    {
        "id": "13",
        "useraccountid": "4",
        "communitytypeid": "2",
        "communityname": "Child of Jamo's Private Place on The Web",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "0",
        "parentcommunity": null,
        "disabled": "N",
        "created_at": "2015-08-17 16:04:06",
        "updated_at": "2015-08-17 16:04:06",
        "deleted_at": null,
        "tree": []
    },
    {
        "id": "14",
        "useraccountid": "4",
        "communitytypeid": "2",
        "communityname": "Real Child of Jamo's Private Place on The Web",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "0",
        "parentcommunity": "2",
        "disabled": "N",
        "created_at": "2015-08-17 16:06:35",
        "updated_at": "2015-08-17 16:06:35",
        "deleted_at": null,
        "tree": []
    },
    {
        "id": "15",
        "useraccountid": "4",
        "communitytypeid": "1",
        "communityname": "Another child of Jamo's space on the web",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "0",
        "parentcommunity": "2",
        "disabled": "N",
        "created_at": "2015-08-17 16:32:44",
        "updated_at": "2015-08-17 16:32:44",
        "deleted_at": null,
        "tree": []
    },
    {
        "id": "17",
        "useraccountid": "4",
        "communitytypeid": "1",
        "communityname": "A legitimate child of Jamo's space on the web",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "0",
        "parentcommunity": "2",
        "disabled": "N",
        "created_at": "2015-08-17 16:33:53",
        "updated_at": "2015-08-17 16:33:53",
        "deleted_at": null,
        "tree": []
    },
    {
        "id": "18",
        "useraccountid": "4",
        "communitytypeid": "1",
        "communityname": "An illegitimate child of Jamo's space on the web",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "1",
        "parentcommunity": "2",
        "disabled": "N",
        "created_at": "2015-08-17 16:35:37",
        "updated_at": "2015-08-17 16:35:37",
        "deleted_at": null,
        "tree": []
    },
    {
        "id": "19",
        "useraccountid": "4",
        "communitytypeid": "1",
        "communityname": "Yet another illegitimate child of Jamo's space on the web",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "1",
        "parentcommunity": "2",
        "disabled": "N",
        "created_at": "2015-08-17 16:40:49",
        "updated_at": "2015-08-17 16:40:49",
        "deleted_at": null,
        "tree": [
            {
                "id": "20",
                "useraccountid": "4",
                "communitytypeid": "1",
                "communityname": "First third gen child under Jamo's line",
                "communitylogo": null,
                "communitycoverphoto": null,
                "communitylevel": "2",
                "parentcommunity": "19",
                "disabled": "N",
                "created_at": "2015-08-17 16:41:31",
                "updated_at": "2015-08-17 16:41:31",
                "deleted_at": null,
                "tree": []
            }
        ]
    },
    {
        "id": "20",
        "useraccountid": "4",
        "communitytypeid": "1",
        "communityname": "First third gen child under Jamo's line",
        "communitylogo": null,
        "communitycoverphoto": null,
        "communitylevel": "2",
        "parentcommunity": "19",
        "disabled": "N",
        "created_at": "2015-08-17 16:41:31",
        "updated_at": "2015-08-17 16:41:31",
        "deleted_at": null,
        "tree": []
    }
];